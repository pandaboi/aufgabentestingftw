# Unicorn Puzzle (Testing FTW)

Es soll ein Unicorn Puzzle erstellt werden.
Die Oberfläche soll mit Hilfe von Swing erstellt werden. 
Im Fokus liegen heute die Unit Tests, die das Programm absichern soll.

TODO: Screenshot vom Spiel hinzufügen.

## 1. Aufgabe (2 Punkte)

### 1.1 Aufgabe (1 Punkt)
Zunächst muss die Entwicklungsumgebung eingerichtet werden.
Ladet das Starterpack hier runter [TODO](link zu Starterpack). 
Das Projekt nutzt [maven](), bei dem Abhängigkeiten in einer `pom.xml` definiert werden.

#### Intellij 
Das Projekt kann neu geöffnet werden, indem ihr danach die `pom.xml` auswählt.

#### Eclipse
Über die `Import` Funktion und dann mit `Existing Maven Project` ausgewählt kann man den Projektordner importieren.

### 1.2 Aufgabe (1 Punkte)
Erstellt die `UnicornPuzzle.java` und schreibt eine eine Testfunktion `add(int x, int y)`, die zwei Zahlen addieren soll.
Danach erstellt ihr die Testklasse `UnicornPuzzleTest.java` (manuell oder mit der IDE) und schreibt dreit Tests für die `add`-Funktion:

* `@Test public void testAdd() {...}`
* `@Test public void testAddNegativeWithPositiveNumber() {...}`
* `@Test public void testAddWithZeroAsParameter() {...}`

(
    Abnahme/Fragen:
    * Abchecken, ob `Mockito` und `JUnit` als Abhängigkeiten drinnen sind und beispielsweise die Annotations gefunden werden.
    * Was haben sich die Menschen bei den drei Tests gedacht - am besten wäre es wenn sie Randfälle, wie 0, negative Zahlen drin haben.  
)

## 2. Aufgabe (2 Punkte) deprecated
Die einzelnen Puzzles des Spiels werden durch Buttons implementiert. Teil dieser Aufgabe ist es diesen Button zu erstellen, der 
auch überpfrüfen kann, ob ein anderer Button sein Nachbar ist.

Notiz: Tests sind nicht Teil dieser Aufgabe.

### 2.1 Aufgabe ( 0,5 Punkt) deprecated
Erstellt die Klasse `PuzzlePiece` in einem gui-`package`. 
Der Button enthält:
* eine Id vom Typ int. Diese Id stellt die Reihenfolge dar. Ist später die Liste der Buttons aufsteigend (nach Id) sortiert,
so gilt das Puzzle als gelöst. Die Lücke erhält immer die id = 0.
* einen Konstruktor mit der Signatur `public PuzzlePiece(int id, ImageIcon icon) `, der Id und das Bild auf dem Button setzt.
* einen Konstruktor mit der Signatur `public PuzzlePiece(int id)` (stellt bei den Konstruktoren sicher, dass diese Randlos sind)

### 2.2 Aufgabe (1,5 Punkte) deprecated
Implementiert folgende Funktion:

```
    /**
     * Check if the other gap/cell is a neighbour 
     * (according to von Neumann https://de.wikipedia.org/wiki/Von-Neumann-Nachbarschaft)
     *
     * @param otherPuzzlePiece
     *         The other PuzzlePiece.
     *
     * @return True if the other PuzzlePiece is a neighbour otherwise false.
     */
    public boolean isNeighbourOf(PuzzlePiece otherPuzzlePiece);
```

Abnahme/Fragen:
    * TODO  

## 3. Aufgabe (8 Punkte)
Die einzelnen Puzzleteile werden in `PuzzlePieces` verwaltet. Die Sammlung soll abgehorcht werden können
und bei Änderungen an dem Puzzleteilen die `Observer` benachrichtigen.

Notiz: Tests sind Teil dieser Aufgabe.

### 3.1 Aufgabe (0,5) deprecated
Klasse `PuzzlePieces` (unter puzzle-`package`) erstellen.

Die Klasse soll folgendes haben/können:
* ein Konstante `EMPTY_GAP_ID`, die den Wert 0 zugewiesen wird (Dies ist die PuzzlePiece id der Lücke)
* eine Liste `puzzlePieces` für `PuzzlePiece`s
* `PuzzlePieces` soll das Interface `Iterable<PuzzlePiece>` implementieren
* `PuzzlePieces` soll observierbar sein

### 3.2 Aufgabe (1 Punkt)
Fügt der Klasse die Funktion `public void add(PuzzlePiece puzzlePiece)` hinzu. Erstellt die dazugehörige Testklasse und einen
Test, der überprüft, dass nachdem ein neues Objekt hinzugefügt wurde auch die Länge der Liste um ein Element zugenommen hat. 

Hinweis: 

* Um reduntante Objektinitialiserungen zu vermeiden soll das gemockte Objekt `PuzzlePieces` in einer Funktion mit `@Before` Annotation initialisiert werden.
*  


### 3.3 Aufgabe (2 Punkt)
Fügt der Klasse die Funktion `public PuzzlePiece getTheGap()` hinzu (noch nicht implementieren).
Danach sollen folgende Tests implementiert werden. Nach jedem Test sollt ihr die Funktion `public PuzzlePiece getTheGap()` 
anpassen, sodass der Test durchläuft (TDD).

* `public void testGetTheGapShouldReturnAvailablePuzzlePieceWithId0()`
* `public void testGetTheGapShouldThrowExceptionForEmptyPuzzle()` 
* `public void testGetTheGapForPuzzleWithNoGaps()`

Hinweis:
Das `PuzzlePiece`, das die Lücke darstellt wird mit durch eine `Id` = 0 definiert.
Daraus folgt das die Funktion `getTheGap` ein `PuzzlePiece` mit der `Id` = 0 zurückliefern (falls vorhanden) soll. 


### 3.4 Aufgabe (3 Punkt)
Fügt der Klasse die Funktion `public void swapPuzzlePieces(int tileIdUno, int tileIdDuo)` hinzu und erstellt folgende Tests:

* `public void testSwapPuzzlePiecesShouldSwapPuzzlePieces()`
* `@Test(expected = IndexOutOfBoundsException.class) public void testSwapPuzzlePiecesShouldThrowExceptionForInvalidIdFirstSwapPartner()`
* `@Test(expected = IndexOutOfBoundsException.class) public void testSwapPuzzlePiecesShouldThrowExceptionForInvalidIdSecondSwapPartner()`
* `public void testSwapPuzzlePiecesShouldCallNotifyObserver()`
* `public void testSwapPuzzlePiecesShouldFireUpdateOnObservers()` 

Hinweis:
Beim letzten Test muss man hier einen Umweg gehen, um indirekt testen zu können, dass `setChanged()` aufgerufen wird, da diese protected ist. Benutzt dafür einen Mock der Oberserver Klasse und fügt diesen dem zu testendem Spy/Mock hinzu. Danach könnt ihr mit `Mockito.verify(...)` überprüfen, ob die update()-Funktion des Observers aufgerufen wurde.

(
    Abnahme/Fragen:
    
    * Tests erklären; welche Testarten (Bheaviour, Randfälle) ist welcher Test?  
    * Wieso habt ihr @Before verwendet?
)

### 3.5 Aufgabe (1,5 Punkte)
Es soll eine weitere Funktion hinzugefügt werden:

```
    /**
     *
     * Get the position in the puzzlePieces array of a tile with the specific id.
     * In case the given id is not part of {@link PuzzlePieces#puzzlePieces} a {@link IllegalArgumentException} is
     * thrown
     *
     * @param id
     *         The tile id.
     *
     * @return The position.
     */
    public int getPosOfPuzzlePieceId(int id)
```

Geht wieder TDD vor und versucht dieses mal selber min. drei sinnvolle Testfälle zu finden. 

Hinweis:
Denkt beispielsweise auch an Ausnahmefälle bzgl. ungültiger Parameter.

### 3.6 Aufgabe (1)
Es soll eine weitere Funktion hinzugefügt werden (inkl. mind. drei dazugehörige Tests):

```
    /**
     * Check if the array is ordered according to the tile id.
     *
     * @return True if the puzzlePieces ArrayList is ordered otherwise false.
     */
    public boolean isOrdered()
```

### 3.7 Aufgabe (1 Punkte)
Es soll eine weitere Funktion hinzugefügt werden (inkl. mind. einen dazugehörigen Test):

```
    /**
     * Shuffle the {@link PuzzlePieces#puzzlePieces} items
     */
    public void shufflePuzzlePieces()
```

## 4. Aufgabe (6,5)

## 4.1 Aufgabe (0,5) deprecated
Fügt dem Projekt das `PuzzlePanel` (gui-`package`) mit seinen Attributen hinzu (gem. Diagram).

## 4.2 Aufgabe (3 Punkte)
Implementiert den Konstruktor mit Hilfe folgender Testfälle:

* `public void testNewPuzzlePanelShouldUseGridLayout()` 
	* Mit dem `instanceOf`- Operator kann verglichen werden, ob es sich bei dem Layout des `PuzzlePanel`s um ein Gridlayout handelt.
* `public void testNewPuzzlePanelShouldSetGridLayoutWithPuzzlePiecesXRows()`
	* Nachdem ersten Test kann bei diesem Test angenommen werden, dass es sich um ein Gridlayout handelt.
* `public void testNewPuzzlePanelShouldSetGridLayoutWithPuzzlePiecesYColumns()`
* `public void testNewPuzzlePanelShouldSetGridLayoutWithZeroGaps()`
* `public void testNewPuzzlePanelShouldAddOneGapPuzzlePieceWithId0()`
	* Erstellt ein `Spy`-Objekt der Klasse `PuzzlePieces` und übergebt es dem Konstruktoraufruf vom `PuzzlePanel`. Danach kann man überprüfen, ob das `Spy`-Objekt eine Lücke mit der Id = 0 hat.

## 4.3 Aufgabe (3 Punkte)
Private `loadImage` Funktion implementieren und im Konstruktor nach dem `add` Aufruf hinzufügen

Indirekte Tests: 

* `public void testNewPuzzlePanelShouldAddPuzzlePiecesWithUniqueIds()`
	*  Der `PuzzlePanel`-Konstruktor erstellt `PuzzlePieces`, die wie bei 4.2 als `Spy`-Objekt dem Konstruktor übergeben werden können.
* `public void testNewPuzzlePanelShouldAddPuzzlePiecesXTimePuzzlePiecesYPuzzlePieces()`
* `public void testNewPuzzlePanelShouldAddPuzzlePiecesWithActionListenerAdded()`

Hinweis: 

* Mit `Mockito.verify()` können Funktionsaufrufe überprüft werden. Um die übergebenen Parameter zu überprüfen kann man u.a. mit Hilfe von `Mockito.any(<DerKlassenname>.class)`, `Mockito.anyObject()`, etc. (siehe nähres unter der Mockito Documentation - auf der mockito.org Seite unter `Latest documentation`, Stichwort: Matcher)
* Einem `JButton` wird ein Bild mit Hilfe der Funktion `setIcon(ImageIcon icon)` hinzugefügt. Um aus einem ganzen Bild einen Bereich auszuschneiden und diesen als `ImageIcon` dem `PuzzlePiece`-Konstruktor zu übergeben sind folgende Funktionen hilfreich:
	* `JPanel`'s `public Image createImage(ImageProducer producer)`-Funktion (von `Component` geerbt)
	* `public FilteredImageSource(ImageProducer orig,
                   ImageFilter imgf)`-Konstruktor, der das Interface `ImageProducer` implementiert und somit der `createImage(..)`-Funktion übergeben werden kann.
	* `public abstract ImageProducer getSource()` liefert den ersten Parameter der für den `FilteredImageSource`-Konstruktor
	* der zweite Parameteter vom `FilteredImageSource`-Konstruktor braucht in diesem Fall einen `CropImageFilter` (bei dem die passende `x` und `y`- Koordinate sowie Höhe und Breite des Auschnitts gesetzt werden muss).
	
		
## 4.4 Aufgabe (2 Punkte)
`public void updatePuzzlePieces(PuzzlePieces puzzlePieces)` mit folgenden Tests impplementieren:

* `public void testUpdatePuzzlePiecesShouldCallRemoveAllElements()`
	* Mit `Mockito.verify()` soll überprüft werden, ob `PuzzlePanel`'s `removeAll()`-Funktion einmal aufgerufen wird. Es sollte beachtet werden, dass die zu testende `updatePuzzlePieces(PuzzlePieces pieces)`-Funktion die `iterator()` und die `add(PuzzlePiece piece)`-Funktion vom übergebenen `PuzzlePieces`aufrufen soll. Nutzt `Mockito.when(...)`, um beim `iterator()`-Aufruf des `PuzzlePieces`-Mocks ein gemocketer Iterator zurückgegeben wird und `doNothing().when(...)` um die `add()`-Funktion vom `PuzzlePieces`-Mock zu ignorieren.
* `public void testUpdatePuzzlePiecesShouldNotExecuteForInvalidParameter()`
	*  Iterator-Hinweis des letzten Tests beachten. Es sollte hier ein `PuzzlePieces`-Objekt mit einigen `PuzzlePiece`s erstellt werden und der zu testenden `updatePuzzlePieces(PuzzlePieces pieces)`-Funktion übergeben werden. Danach kann mit `überprüft` werden, wie oft die `add(..)`-Funktion mit dem Testdaten(zuvor erstellten `PuzzlePiece` in `PuzzlePieces`) aufgerufen wurde.
* `public void testUpdatePuzzlePiecesShouldOnlyAddAllGivenPuzzlePieces()`
	*  Iterator-Hinweis des vorletzten Tests beachten.

## 5. Aufgabe (0,5) deprecated
Als nächstes soll ein schlichtes `WinnersDialog` implementiert werden, welches anzeigt, dass man gewonnen hat. Es müssen hier keine Tests dazu erstellt werden.

## 6. Aufgabe (1,5 Punkte) deprecated
Als letztes soll das `PuzzleFrame` nach dem Diagram (TODO) erstellt werden.

Notiz: Tests sind nicht Teil der Aufgabe.

### 6.1 Aufgabe (0,5 Punkte)
Implementiere den Konstruktor mit folgenden Kriterien:
* `GAME_TITLE` soll der Titel sein
* `puzzlePieces` soll observiert werden können
* ein `PuzzlePanel` soll dem Frame hinzugefügt werden
* die Größe des Fensters soll alle Buttons darstellen können und nicht mehr veränderbar sein
* Fenster soll schliessbar sein

In der `update`-Funktion soll das `puzzlePanel` die `puzzlePieces` updaten und das Fenster muss danach neu gerendert werden.


### 6.2 Aufgabe (1 Punkt)
Als letztes muss noch die `actionPerformed` implementiert werden:
Falls das `ActionEvent` ein JButton vom Typ `PuzzlePiece` ist und dieser ein Nachbar von der Lücke ist, soll dieses Puzzleteil in die Lücke verschoben werden.

Nach jedem Zug muss überprüft werden, ob das Puzzle gelöst wurde. Bei einem Erfolg soll das `WinnersDialog` erscheinen.


Happy Coding